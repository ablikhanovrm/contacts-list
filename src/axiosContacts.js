import axios from "axios";

const axiosContacts = axios.create({
    baseURL: 'https://contact-list-8ce81-default-rtdb.firebaseio.com/'
});

export default axiosContacts;