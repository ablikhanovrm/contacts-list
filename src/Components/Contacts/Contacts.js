import React from "react";
import Contact from "./Contact/Contact";
import './Contacts.css';
const Contacts = ({allContacts, infoHandler}) => {

    return (
        <div className="contacts">
                {allContacts.map(cont => {
                    return(
                        <Contact
                            key={cont.id}
                            showInfo={() => {infoHandler(cont.id)}}
                            cont={cont} 
                        />
                    )
                })}
        </div>
    )
}

export default Contacts;