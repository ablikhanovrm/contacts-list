import React from "react";
import './MyButton.css';

const SaveButton = ({clicked, isDisabled, text}) =>  {

        return (
            <button disabled={isDisabled} onClick={clicked} className="MyButton">{text}</button>
        )
    

}

export default SaveButton;