import React, { useState, useEffect } from "react";
import Spinner from "../../Components/Spinner/Spinner";
import { shallowEqual, useSelector, useDispatch } from "react-redux";
import './CreateContact.css';
import { createContact } from "../../Store/contactSlice";
import { useNavigate } from "react-router-dom";
import MyButton from "../../Components/Button/MyButton";


const CreateContact = () => {

    const {isLoading} = useSelector(state => state.contacts, shallowEqual)
    const [isSelect, setIsSelect] = useState(true)
    const dispatch = useDispatch();
    const navigate = useNavigate();

    const [formValue, setFormValue] = useState({
        name:'',
        phone:'',
        email: '',
        image: '',
    });


    useEffect( () => {
        if(
        formValue.title === "" 
        || formValue.price === "" 
        || formValue.image === "" 
        || formValue.email === ""
        ){
            setIsSelect(false)
        }else {
            setIsSelect(true)
        }
    }, [formValue]);


    const changeHandler = e => {
        const {name, value} = e.target;
        setFormValue({...formValue, [name]: value});
    }

    const createCont = async (e) => {
        console.log('вызвал создание контакта');
        e.preventDefault();
        await dispatch(createContact(formValue))
        navigate('/')
    } 
   
    return (
        <>
            <div className="CreatePage">
                <h1 className="adminTitle">Submit new Contact</h1>
                <form onSubmit={createCont}>
                        <div className="changePageInfo">
                            <h3 className="pageTitle">Contact name:</h3>
                            <input 
                                onChange={changeHandler} 
                                type="text" name="name" 
                                value={formValue.name} 
                                className="InputText"
                                placeholder="Укажите имя контакта..."
                            />
                            
                            <h3 className="pageContent">Contact phone:</h3>
                            <input 
                                onChange={changeHandler}
                                className="InputText" 
                                name="phone" value={formValue.phone} 
                                placeholder="Введите "
                            />
                            <h3 className="pageContent">Contact email:</h3>
                            <input 
                                onChange={changeHandler}
                                className="InputText"
                                name="email" value={formValue.email} 
                                placeholder="Укажите email контакта..."
                            />
                            <h3 className="pageContent">Contact photo:</h3>
                            <input 
                                onChange={changeHandler}
                                className="InputText"
                                name="image" value={formValue.image} 
                                placeholder="Вставьте ссылку на картинку..."
                            />
                        </div>
                        <img className="contactPhoto" src={formValue.image} alt="ContactImg" />
                        <MyButton
                            text={`Save`}
                            isDisabled={!isSelect}
                            clicked={createCont}
                        />
                        <MyButton
                            text={`Back to Contacts`}
                            clicked={() => {navigate('/')}}
                        />
                </form>
            </div>
    {isLoading ? <Spinner/> : null}
    </>
    )
}
export default CreateContact;