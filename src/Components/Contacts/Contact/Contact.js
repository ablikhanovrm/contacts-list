import React from "react";
import './Contact.css';

const Contact = ({cont, showInfo}) => {
    return (
        <div onClick={showInfo} className="contact">
            <img className="contImg" src={cont.image} alt="ContactImg" />
            <h3 className="contname">{cont.name}</h3>
        </div>
    )
}

export default Contact;