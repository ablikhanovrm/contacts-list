import React from "react";
import NavBar from "../NavBar/NavBar";
import { Outlet } from "react-router";

const Layout = () => {

    return (
        <>
            <NavBar/>
            <div className="main">
                <Outlet/>
            </div>        
        </>
    )
}

export default Layout;