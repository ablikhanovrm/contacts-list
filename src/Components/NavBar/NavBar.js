import React from "react";
import { NavLink } from "react-router-dom";
import './NavBar.css';

const NavBar = () => {
    return (
        <header className="header">
            <nav className="navBar">
                <NavLink className="navLink LinkContacts" to="/">Contacts</NavLink>
                <NavLink className="navLink LinkAdd" to="add-contact">Add new contact</NavLink>
            </nav>
        </header>
    )
}

export default NavBar;