import React, {useEffect} from "react";
import './HomePage.css';
import { shallowEqual, useSelector } from "react-redux";
import { useDispatch } from "react-redux";
import { getAllContacts } from "../../Store/contactSlice";
import Contacts from "../../Components/Contacts/Contacts";
import { showModal, closeModal } from "../../Store/contactSlice";
import Modal from "../../Components/Modal/Modal";
import { deleteContact } from "../../Store/contactSlice";
import Spinner from "../../Components/Spinner/Spinner";

const HomePage = () => {

    const {allContacts, isModal, isLoading} = useSelector(state => state.contacts, shallowEqual)
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(getAllContacts())
    }, [dispatch])

    const showInfoHandler = (id) => {
        dispatch(showModal(id))
}

    const closeModalHandler = () => {
        dispatch(closeModal())
    }

    const deleteContactHandler = (id) => {
        dispatch(deleteContact(id))
    }

    return (
        <div className="homePage container">
                <Contacts
                    allContacts={allContacts}
                    infoHandler={showInfoHandler}
                />
                {isModal ?
                <Modal 
                    closeModalWindow={closeModalHandler}
                    deleteCont={deleteContactHandler}
                />
                : null}
                {isLoading ? <Spinner/> : null}
        </div>
    ) 

}

export default HomePage;