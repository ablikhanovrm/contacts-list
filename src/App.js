import React from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Layout from './Components/Layout/Layout';
import HomePage from "./Containers/HomePage/HomePage";
import CreateContact from "./Containers/CreateContact/CreateContact";
import EditContact from "./Containers/EditContact/EditContact";

const App = () => {
  return (
    <BrowserRouter>
    <Routes>
      <Route path="/" element={<Layout/>}>
          <Route index element={<HomePage/>}/>
          <Route path="add-contact" element={<CreateContact/>}/>
          <Route path="/contacts/:id/edit" element={<EditContact/>}/>
      </Route>
    </Routes>  
  </BrowserRouter>    
  );
}

export default App;
