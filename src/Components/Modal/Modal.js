import React from "react";
import { shallowEqual, useSelector, useDispatch } from "react-redux";
import MyButton from "../../Components/Button/MyButton";
import { useNavigate } from "react-router-dom";
import { closeModal } from "../../Store/contactSlice";
import './Modal.css';

const Modal = ({closeModalWindow, deleteCont}) => {

    const {contInfo} = useSelector(state => state.contacts, shallowEqual);    
    const navigate = useNavigate();
    const dispatch= useDispatch();

    const editContactHandler = () => {
        navigate(`/contacts/${contInfo.id}/edit`);
        dispatch(closeModal())
    }

    return (
        <div className="modal">
            <div className="modalContainer">
                <img className="ModalImg" src={contInfo.image} alt="ContactImage"/>
                <div className="contact_Info">
                    <h2 className="contName">{contInfo.name}</h2>
                    <span className="contData"><strong>Phone:</strong> {contInfo.phone}</span>
                    <span className="contData"><strong>Email:</strong> {contInfo.email}</span>
                </div>    
            </div>
            <div className="controlButtons">
                <MyButton 
                    text={`Edit`}
                    clicked={editContactHandler}
                />
                <MyButton 
                    text={`Delete`}
                    clicked={() => {deleteCont(contInfo.id)}}
                />
                <button onClick={closeModalWindow} className="closeButton">Close</button>
            </div>
        </div>
    )
}

export default Modal;