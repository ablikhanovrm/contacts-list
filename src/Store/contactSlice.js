import { createSlice, createAsyncThunk} from "@reduxjs/toolkit";
import axiosContacts from "../axiosContacts";


const initialState = {
    allContacts: [],
    isLoading: false,
    error: null,
    contInfo: null,
    isModal: false,
    foundContact: null,
    name:'',
    phone: '',
    email: '',
    image: '',
}

export const createContact = createAsyncThunk(
    'contacts/createContact',
    async (payload) => {
        await axiosContacts.post('contacts.json', payload);
    }
    )
    
export const getAllContacts = createAsyncThunk(
    'contacts/getcontacts',
    async () => {
        const res = await axiosContacts('contacts.json');
        return res.data;
    }
)

export const findContact = createAsyncThunk(
    'contacts/findContact',
    async (payload) => {
        const res = await axiosContacts(`contacts/${payload}.json`);
        return res.data;
    }
)

export const editContact = createAsyncThunk(
    'contacts/editContact',
    async (payload) => {
        await axiosContacts.put(`contacts/${payload.id}.json`, 
        {name: payload.name, phone:payload.phone, email: payload.email, image: payload.image})
    }
)

export const deleteContact = createAsyncThunk(
    'contacts/deleteContact',
    async (payload) => {
        await axiosContacts.delete(`contacts/${payload}.json`);
        return payload;
    }
)

const contSlice = createSlice({
    name: 'contSlice', 
    initialState,
    reducers: {
        showModal: (state, action) => {
            const index = state.allContacts.findIndex(c => c.id === action.payload);
            state.contInfo = state.allContacts[index];
            state.isModal = true;
        },
        closeModal: (state) => {
            state.isModal = false;
        },
        changeFormValue: (state, action) => {
            state[action.payload.name] = action.payload.value;
        }
    },
    extraReducers: builder => {
        builder
        .addCase(getAllContacts.pending, state => {
            state.isLoading = true;
        })
        .addCase(getAllContacts.rejected, (state, action) => {
            state.error = action.error;
            state.isLoading = false;
        })
        .addCase(getAllContacts.fulfilled, (state, action) => {
            if(action.payload === null) {
                state.isLoading = false;
            } else{
                const fetchedContacts = Object.keys(action.payload).map(id => {
                    return {...action.payload[id], id};
                });
                state.isLoading = false;
                state.allContacts = fetchedContacts;
            }
        })
        .addCase(createContact.pending, state => {
            state.isLoading = true;
        })
        .addCase(createContact.rejected, (state, action) => {
            state.error = action.error;
            state.isLoading = false;
        })
        .addCase(createContact.fulfilled, (state) => {
            state.isLoading = false;
        })
        .addCase(findContact.pending, state => {
            state.isLoading = true;
        })
        .addCase(findContact.rejected, (state, action) => {
            state.error = action.error;
            state.isLoading = false;
        })
        .addCase(findContact.fulfilled, (state, action) => {
            state.foundСontact = action.payload;
            state.name = action.payload.name;
            state.phone = action.payload.phone;
            state.email = action.payload.email;
            state.image = action.payload.image;
            state.isLoading = false;
        })
        .addCase(editContact.pending, state => {
            state.isLoading = true;
        })
        .addCase(editContact.rejected, (state, action) => {
            state.error = action.error;
            state.isLoading = false;
        })
        .addCase(editContact.fulfilled, (state) => {
            state.isLoading = false;
        })
        .addCase(deleteContact.pending, state => {
            state.isLoading = true;
        })
        .addCase(deleteContact.rejected, (state, action) => {
            state.error = action.error;
            state.isLoading = false;
        })
        .addCase(deleteContact.fulfilled, (state, action) => {
            const index = state.allContacts.findIndex(c => c.id === action.payload)
            state.allContacts.splice(index, 1);
            state.isLoading = false;
            state.isModal = false;
        }) 
    }
})

export const {showModal, closeModal, changeFormValue} = contSlice.actions;
export default contSlice.reducer;