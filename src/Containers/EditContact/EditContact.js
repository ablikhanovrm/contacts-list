import React, { useState, useEffect } from "react";
import Spinner from "../../Components/Spinner/Spinner";
import MyButton from "../../Components/Button/MyButton";
import { useNavigate, useParams } from "react-router-dom";
import { shallowEqual, useSelector, useDispatch } from "react-redux";
import { findContact, changeFormValue, editContact } from "../../Store/contactSlice";
import './EditContact.css';

const EditContact = () => {

    const {isLoading, name, email, phone, image} = useSelector(state => state.contacts, shallowEqual);
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const params = useParams();

    const [isSelect, setIsSelect] = useState(true)

    useEffect( () => {
        if(name === "" || email === "" ||
        phone === "" || image === ""){
            setIsSelect(false)
        }else {
            setIsSelect(true)
        }
    }, [name, email, phone, image]);

    useEffect( () => {
        dispatch(findContact(params.id))
    }, [dispatch, params.id])


    const changeHandler = e => {
        const payload = {name: e.target.name, value: e.target.value};
        dispatch(changeFormValue(payload))
      }

    const changeContact = async (e) => {
        e.preventDefault();
        const contact = {
            id: params.id,
            name: name,
            phone: phone,
            email: email,
            image: image,
        }
        await dispatch(editContact(contact));
        navigate('/');
    } 

   
    return (
        <>
        <div className="CreatePage">
            <h1 className="adminTitle">Submit new Contact</h1>
            <div className="changePageInfo">
                <form >
                    <h3 className="pageTitle">Contact name:</h3>
                    <input onChange={changeHandler} 
                    type="text" name="name" 
                    value={name} 
                    className="InputText"
                    placeholder="Укажите имя контакта..."/>
                    
                    <h3 className="pageContent">Contact phone:</h3>
                    <input onChange={changeHandler}
                    className="InputText" 
                    name="phone" value={phone} 
                    placeholder="Введите "
                    />
                    <h3 className="pageContent">Contact email:</h3>
                    <input onChange={changeHandler}
                    className="InputText"
                    name="email" value={email} 
                    placeholder="Укажите email контакта..."
                    />
                    <h3 className="pageContent">Contact photo:</h3>
                    <input onChange={changeHandler}
                    className="InputText"
                    name="image" value={image} 
                    placeholder="Вставьте ссылку на картинку..."
                    />
            </form>
        </div>
            <img className="contactPhoto" src={image} alt="" />
            <MyButton
             text={`Back to Contacts`}
             clicked={() => {navigate('/')}}
            />
            <MyButton
                text={`Save`}
                isDisabled={!isSelect}
               clicked={changeContact}
            />
        </div>
    {isLoading ? <Spinner/> : null}
    </>
    )
}
export default EditContact;